package ledgerbook

type Response struct {
	LedgerBook LedgerBook `json:"ledgerbook"`
	Report     string     `json:"report"`
}

type ResponseBadRequest struct {
	Message string `json:"message"`
}

type ResponseAllLedgerBooks struct {
	LedgerBooks []LedgerBook `json:"ledgerbooks"`
}
