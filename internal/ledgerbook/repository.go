package ledgerbook

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
)

var Collection = "visitors"

type Repository struct {
	Collection *mongo.Collection
}

func NewRepository(c *mongo.Collection) *Repository {
	return &Repository{
		Collection: c,
	}
}

func (r *Repository) InsertOneToDatabase(ctx context.Context, l *LedgerBook) (*mongo.InsertOneResult, error) {
	result, err := r.Collection.InsertOne(ctx, l)
	if err != nil {
		log.Fatal(err)
	}

	return result, err
}

func (r *Repository) GetOneRecordFromDatabaseByInsertResult(ctx context.Context, insertResult *mongo.InsertOneResult) *LedgerBook {
	var ledgerbook LedgerBook
	err := r.Collection.FindOne(ctx, bson.M{"_id": insertResult.InsertedID}).Decode(&ledgerbook)

	if err != nil {
		log.Fatal(err)
	}

	return &ledgerbook
}

func (r *Repository) InsertLedgerBook(ctx context.Context, l *LedgerBook) *LedgerBook {
	insertResult, err := r.InsertOneToDatabase(ctx, l)
	if err != nil {
		log.Fatal(err)
	}

	result := r.GetOneRecordFromDatabaseByInsertResult(ctx, insertResult)

	return result
}

func (r *Repository) GetAllRecordsFromDatabase(ctx context.Context) *[]LedgerBook {
	cur, err := r.Collection.Find(ctx, bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	var results []LedgerBook

	if err = cur.All(ctx, &results); err != nil {
		log.Fatal(err)
	}

	return &results
}
