package ledgerbook

import (
	"context"
)

type (
	ServiceRepo interface {
		InsertLedgerBook(ctx context.Context, l *LedgerBook) *LedgerBook
		GetAllRecordsFromDatabase(ctx context.Context) *[]LedgerBook
	}
	Service struct {
		Repository ServiceRepo
	}
)

const SuccessReport = "New Ledgerbook added successfully!"

func NewService(s ServiceRepo) *Service {
	return &Service{
		Repository: s,
	}
}

func (s *Service) AddNewLedgerBook(ctx context.Context, l *LedgerBook) *Response {
	result := s.Repository.InsertLedgerBook(ctx, l)
	response := Response{
		LedgerBook: *result,
		Report:     SuccessReport,
	}
	return &response
}

func (s *Service) GetAllLedgerBook(ctx context.Context) *ResponseAllLedgerBooks {
	result := s.Repository.GetAllRecordsFromDatabase(ctx)
	response := ResponseAllLedgerBooks{
		LedgerBooks: *result,
	}
	return &response
}