package ledgerbook

import (
	"context"
	"github.com/gofiber/fiber/v2"
)

type (
	HandlerService interface {
		AddNewLedgerBook(ctx context.Context, l *LedgerBook) *Response
		GetAllLedgerBook(ctx context.Context) *ResponseAllLedgerBooks
	}
	Handler struct {
		Service HandlerService
	}
)

func NewHandler(h HandlerService) *Handler {
	return &Handler{
		Service: h,
	}
}

var BadRequest = ResponseBadRequest{
	Message: "Failed to parse body",
}

func (h *Handler) RegisterRoutes(app *fiber.App) {
	api := app.Group("/ledgerbook")

	api.Post("/newledgerbook", h.AddNewLedgerBook)
	api.Get("/ledgerbooks", h.GetAllLedgerBook)
}

func (h *Handler) AddNewLedgerBook(c *fiber.Ctx) error {
	var ledgerbook LedgerBook

	if err := c.BodyParser(&ledgerbook); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(BadRequest)
	}

	response := h.Service.AddNewLedgerBook(c.Context(), &ledgerbook)
	return c.Status(fiber.StatusCreated).JSON(response)
}

func (h *Handler) GetAllLedgerBook(c *fiber.Ctx) error {
	response := h.Service.GetAllLedgerBook(c.Context())
	return c.Status(fiber.StatusOK).JSON(response)
}
