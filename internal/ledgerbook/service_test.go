//go:build unit_tests
// +build unit_tests

package ledgerbook_test

import (
	"context"
	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	l "ledgerbook/internal/ledgerbook"
	mocks "ledgerbook/internal/mocks/ledgerbook"
	"testing"
	"time"
)

func TestAddNewLedgerBook(t *testing.T) {
	newLedgerBook := l.LedgerBook{
		Email:   "testService@email.com",
		Message: "testService message",
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mocks.NewMockServiceRepo(ctrl)
	mockRepo.EXPECT().InsertLedgerBook(gomock.Any(), &newLedgerBook).Return(&newLedgerBook)

	service := l.NewService(mockRepo)

	expectedServiceResponse := l.Response{
		LedgerBook: newLedgerBook,
		Report:     l.SuccessReport,
	}

	result := service.AddNewLedgerBook(ctx, &newLedgerBook)

	assert.Equal(t, expectedServiceResponse.LedgerBook, result.LedgerBook)
	assert.Equal(t, expectedServiceResponse.Report, result.Report)
}

func TestGetAllLedgerBook(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockLedgerBook := l.LedgerBook{
		Email:   "mock@email.com",
		Message: "mock message",
	}

	mockResponse := []l.LedgerBook{mockLedgerBook}

	mockRepo := mocks.NewMockServiceRepo(ctrl)
	mockRepo.EXPECT().GetAllRecordsFromDatabase(gomock.Any()).Return(&mockResponse)

	service := l.NewService(mockRepo)

	expectedResponse := l.ResponseAllLedgerBooks{
		LedgerBooks: mockResponse,
	}

	result := service.GetAllLedgerBook(ctx)

	assert.Equal(t, expectedResponse, *result)
}
