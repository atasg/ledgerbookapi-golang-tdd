//go:build unit_tests
// +build unit_tests

package ledgerbook_test

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	l "ledgerbook/internal/ledgerbook"
	mocks "ledgerbook/internal/mocks/ledgerbook"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestWhenValidLedgerBookIsGivenItShouldReturnStatusCreated(t *testing.T) {
	app := fiber.New()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	newLedgerBook := l.LedgerBook{
		Email:   "handlertest@email.com",
		Message: "handler test message",
	}

	mockResponse := l.Response{
		LedgerBook: newLedgerBook,
		Report:     l.SuccessReport,
	}
	mockService := mocks.NewMockHandlerService(ctrl)
	mockService.EXPECT().AddNewLedgerBook(gomock.Any(), &newLedgerBook).Return(&mockResponse)

	handler := l.NewHandler(mockService)
	handler.RegisterRoutes(app)

	toSendBody, _ := json.Marshal(newLedgerBook)
	body := bytes.NewBuffer(toSendBody)
	request := httptest.NewRequest(http.MethodPost, "/ledgerbook/newledgerbook", body)
	request.Header.Add("Content-Type", "application/json")

	response, _ := app.Test(request)
	responseBody, _ := ioutil.ReadAll(response.Body)

	var resp l.Response
	json.Unmarshal(responseBody, &resp)

	assert.Equal(t, http.StatusCreated, response.StatusCode)
	assert.Equal(t, mockResponse, resp)
}

func TestWhenBadJsonInputIsGivenItShouldReturnBadRequest(t *testing.T) {
	app := fiber.New()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockService := mocks.NewMockHandlerService(ctrl)
	handler := l.NewHandler(mockService)
	handler.RegisterRoutes(app)

	r := strings.NewReader("bad request")
	request := httptest.NewRequest(http.MethodPost, "/ledgerbook/newledgerbook", r)
	request.Header.Add("Content-Type", "application/json")
	response, _ := app.Test(request)

	assert.Equal(t, http.StatusBadRequest, response.StatusCode)

	responseBody, _ := ioutil.ReadAll(response.Body)
	var resp l.ResponseBadRequest
	json.Unmarshal(responseBody, &resp)

	assert.Equal(t, l.BadRequest, resp)
}

func TestWhenGetAllLedgerBoookIsCalledItShouldReturnEveryDocument(t *testing.T) {
	app := fiber.New()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockService := mocks.NewMockHandlerService(ctrl)

	newLedgerBook := l.LedgerBook{
		Email:   "handlertest@email.com",
		Message: "handler test message",
	}
	ledgerList := []l.LedgerBook{newLedgerBook}
	mockResponse := l.ResponseAllLedgerBooks{
		LedgerBooks: ledgerList,
	}

	mockService.EXPECT().GetAllLedgerBook(gomock.Any()).Return(&mockResponse)
	handler := l.NewHandler(mockService)
	handler.RegisterRoutes(app)

	request := httptest.NewRequest(http.MethodGet, "/ledgerbook/ledgerbooks", http.NoBody)
	request.Header.Add("Content-Type", "application/json")

	response, _ := app.Test(request)
	assert.Equal(t, http.StatusOK, response.StatusCode)

	responseBody, _ := ioutil.ReadAll(response.Body)
	var resp l.ResponseAllLedgerBooks
	json.Unmarshal(responseBody, &resp)

	assert.Equal(t, mockResponse, resp)
}
