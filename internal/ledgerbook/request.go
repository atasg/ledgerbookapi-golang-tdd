package ledgerbook

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type LedgerBook struct {
	ID      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Email   string             `json:"email,omitempty" bson:"email,omitempty"`
	Message string             `json:"message,omitempty" bson:"message,omitempty"`
}
