//go:build database_tests
// +build database_tests

package ledgerbook_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"ledgerbook/config"
	l "ledgerbook/internal/ledgerbook"
	"log"
	"testing"
	"time"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"github.com/docker/go-connections/nat"

	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func TestInsertOneToDatabase(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
	defer cancel()

	container, MI := dbSetup(t)
	defer container.Terminate(ctx)

	collection := MI.DB.Collection(l.Collection)

	repository := l.NewRepository(collection)
	
	newLedgerbook := l.LedgerBook{
		Email:   "dummy@email.com",
		Message: "dummy message",
	}

	result, errInsert := repository.InsertOneToDatabase(ctx, &newLedgerbook)

	assert.NoError(t, errInsert)

	var addedLedgerbook l.LedgerBook

	if err := collection.FindOne(ctx, bson.M{"_id": result.InsertedID}).Decode(&addedLedgerbook); err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, newLedgerbook.Email, addedLedgerbook.Email)
	assert.Equal(t, newLedgerbook.Message, addedLedgerbook.Message)
}

func TestGetOneRecordFromDatabaseByInsertId(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
	defer cancel()

	container, MI := dbSetup(t)
	defer container.Terminate(ctx)

	collection := MI.DB.Collection(l.Collection)
	repository := l.NewRepository(collection)

	newLedgerbook := l.LedgerBook{
		Email:   "test@email.com",
		Message: "test message",
	}

	insertResult, _ := repository.Collection.InsertOne(ctx, newLedgerbook)

	result := repository.GetOneRecordFromDatabaseByInsertResult(ctx, insertResult)

	assert.Equal(t, newLedgerbook.Email, result.Email)
	assert.Equal(t, newLedgerbook.Message, result.Message)
}

func TestInsertLedgerBook(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
	defer cancel()

	container, MI := dbSetup(t)
	defer container.Terminate(ctx)

	collection := MI.DB.Collection(l.Collection)
	repository := l.NewRepository(collection)

	newLedgerbook := l.LedgerBook{
		Email:   "ledgerbook@email.com",
		Message: "ledgerbook message",
	}

	result := repository.InsertLedgerBook(ctx, &newLedgerbook)

	assert.Equal(t, newLedgerbook.Email, result.Email)
	assert.Equal(t, newLedgerbook.Message, result.Message)
}

func TestGetAllRecordsFromDatabase(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
	defer cancel()

	container, MI := dbSetup(t)
	defer container.Terminate(ctx)

	collection := MI.DB.Collection(l.Collection)

	size, err := MI.DB.Collection(l.Collection).CountDocuments(ctx, bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	repository := l.NewRepository(collection)

	allVisitors := repository.GetAllRecordsFromDatabase(ctx)

	assert.NoError(t, err)

	sizeOfAll := len(*allVisitors)
	sizeDatabase := int(size)
	assert.Equal(t, sizeDatabase, sizeOfAll)
} 

func dbSetup(t *testing.T) (testcontainers.Container, config.MongoInstance) {
	port:= nat.Port("27017/tcp")

	ctx := context.Background()
	startupTimeout := 120 * time.Second

	req := testcontainers.ContainerRequest{
		Image: "mongo:latest",
		ExposedPorts: []string{
			"27017:27017/tcp",
		},
		WaitingFor: wait.ForListeningPort(port).WithStartupTimeout(startupTimeout),
		Env: map[string]string{
			"MONGO_INITDB_ROOT_USERNAME":"root",
			"MONGO_INITDB_ROOT_PASSWORD":"example",
		},
	}

	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
        ContainerRequest: req,
        Started:          true,
    })
	assert.NoError(t, err)

	ip, err := container.Host(ctx)
	assert.NoError(t, err)

	mappedPort, err := container.MappedPort(ctx, port)
	assert.NoError(t, err)

	uri := fmt.Sprintf("mongodb://root:example@%s:%s", ip, mappedPort.Port())

	clientOptions := options.Client().ApplyURI(uri)
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
	defer cancel()

	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Database connected!")

	MI := config.MongoInstance{
		Client: client,
		DB:     client.Database("ledgerbook"),
	}
	return container, MI
}