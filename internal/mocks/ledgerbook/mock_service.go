// Code generated by MockGen. DO NOT EDIT.
// Source: ./internal/ledgerbook/handler.go

// Package mock_ledgerbook is a generated GoMock package.
package mock_ledgerbook

import (
	context "context"
	ledgerbook "ledgerbook/internal/ledgerbook"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockHandlerService is a mock of HandlerService interface.
type MockHandlerService struct {
	ctrl     *gomock.Controller
	recorder *MockHandlerServiceMockRecorder
}

// MockHandlerServiceMockRecorder is the mock recorder for MockHandlerService.
type MockHandlerServiceMockRecorder struct {
	mock *MockHandlerService
}

// NewMockHandlerService creates a new mock instance.
func NewMockHandlerService(ctrl *gomock.Controller) *MockHandlerService {
	mock := &MockHandlerService{ctrl: ctrl}
	mock.recorder = &MockHandlerServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockHandlerService) EXPECT() *MockHandlerServiceMockRecorder {
	return m.recorder
}

// AddNewLedgerBook mocks base method.
func (m *MockHandlerService) AddNewLedgerBook(ctx context.Context, l *ledgerbook.LedgerBook) *ledgerbook.Response {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddNewLedgerBook", ctx, l)
	ret0, _ := ret[0].(*ledgerbook.Response)
	return ret0
}

// AddNewLedgerBook indicates an expected call of AddNewLedgerBook.
func (mr *MockHandlerServiceMockRecorder) AddNewLedgerBook(ctx, l interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddNewLedgerBook", reflect.TypeOf((*MockHandlerService)(nil).AddNewLedgerBook), ctx, l)
}

// GetAllLedgerBook mocks base method.
func (m *MockHandlerService) GetAllLedgerBook(ctx context.Context) *ledgerbook.ResponseAllLedgerBooks {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAllLedgerBook", ctx)
	ret0, _ := ret[0].(*ledgerbook.ResponseAllLedgerBooks)
	return ret0
}

// GetAllLedgerBook indicates an expected call of GetAllLedgerBook.
func (mr *MockHandlerServiceMockRecorder) GetAllLedgerBook(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAllLedgerBook", reflect.TypeOf((*MockHandlerService)(nil).GetAllLedgerBook), ctx)
}
