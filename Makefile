database_test:
	go clean -testcache && go test -tags=database_tests ./... -v

unit_test:
	go clean -testcache && go test -tags=unit_tests ./... -v

db_connection_test:
	go clean -testcache && go test -tags=database_connection_test ./... -v
	
all_test: unit_test database_test db_connection_test
