# BUILD
FROM golang:latest

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /app
ADD . /app

COPY go.mod .
COPY go.sum .

RUN go mod download 

COPY . .

RUN go build -o ledgerbook

CMD ["./ledgerbook"]

EXPOSE 8080

