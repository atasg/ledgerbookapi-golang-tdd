package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"ledgerbook/config"
	l "ledgerbook/internal/ledgerbook"
	"log"
)

func main() {
	config.ConnectDB()
	app := CreateApp()
	log.Fatal(app.Listen(":3000"))
}

func CreateApp() *fiber.App {
	app := fiber.New()
	collection := config.MI.DB.Collection(l.Collection)
	repository := l.NewRepository(collection)
	service := l.NewService(repository)
	handler := l.NewHandler(service)

	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowHeaders:     "*",
		AllowCredentials: true,
	}))
	handler.RegisterRoutes(app)
	return app
}
